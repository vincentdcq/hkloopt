# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Home.create!(naam: "Konvent",
			 password: "kmP-nYM-evo-A3t",
			 password_confirmation: "kmP-nYM-evo-A3t",
			 admin: true)
Home.create!(naam: "Astrid",
			 password: "K4d-Vsm-kUM-AXP",
			 password_confirmation: "K4d-Vsm-kUM-AXP",
			 loopt: true)
Home.create!(naam: "Bertha",
			 password: "Tdm-3Lq-Hgb-Dnk",
			 password_confirmation: "Tdm-3Lq-Hgb-Dnk")
Home.create!(naam: "Boudewijn",
			 password: "caM-5fs-7PA-nMn",
			 password_confirmation: "caM-5fs-7PA-nMn")
Home.create!(naam: "Confabula",
			 password: "AE2-Pnr-m3o-XUA",
			 password_confirmation: "AE2-Pnr-m3o-XUA")
Home.create!(naam: "Fabiola",
			 password: "MCW-v4T-Wkn-U5v",
			 password_confirmation: "MCW-v4T-Wkn-U5v")
Home.create!(naam: "Savania",
			 password: "hav-EuZ-5yY-rpX",
			 password_confirmation: "hav-EuZ-5yY-rpX")
Home.create!(naam: "Vermeylen",
			 password: "9QA-eMv-zvb-Dvk",
			 password_confirmation: "9QA-eMv-zvb-Dvk")