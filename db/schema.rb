# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180423144350) do

  create_table "homes", force: :cascade do |t|
    t.string "naam"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "password_digest"
    t.string "remember_digest"
    t.boolean "admin", default: false
    t.integer "speciaal", default: 0
    t.boolean "loopt"
  end

  create_table "lopers", force: :cascade do |t|
    t.string "naam"
    t.integer "studentennummer"
    t.integer "home_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "snelste", default: 1000
    t.integer "aantal", default: 0
    t.boolean "aangemeld", default: false
    t.datetime "aangemeld_op"
    t.index ["home_id"], name: "index_lopers_on_home_id"
  end

end
