class AddSnelsteToLopers < ActiveRecord::Migration[5.1]
  def change
    add_column :lopers, :snelste, :integer, default: 1000
  end
end
