class AddAantalToLopers < ActiveRecord::Migration[5.1]
  def change
    add_column :lopers, :aantal, :integer, default: 0
  end
end
