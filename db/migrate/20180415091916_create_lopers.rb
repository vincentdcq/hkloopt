class CreateLopers < ActiveRecord::Migration[5.1]
  def change
    create_table :lopers do |t|
      t.string :naam
      t.integer :studentennummer
      t.references :home, foreign_key: true

      t.timestamps
    end
  end
end
