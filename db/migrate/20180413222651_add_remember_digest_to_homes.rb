class AddRememberDigestToHomes < ActiveRecord::Migration[5.1]
  def change
    add_column :homes, :remember_digest, :string
  end
end
