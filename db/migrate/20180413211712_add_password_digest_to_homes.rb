class AddPasswordDigestToHomes < ActiveRecord::Migration[5.1]
  def change
    add_column :homes, :password_digest, :string
  end
end
