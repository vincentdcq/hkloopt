class AddSpeciaalToHomes < ActiveRecord::Migration[5.1]
  def change
    add_column :homes, :speciaal, :integer, default: 0
  end
end
