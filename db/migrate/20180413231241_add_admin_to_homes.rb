class AddAdminToHomes < ActiveRecord::Migration[5.1]
  def change
    add_column :homes, :admin, :boolean, default: false
  end
end
