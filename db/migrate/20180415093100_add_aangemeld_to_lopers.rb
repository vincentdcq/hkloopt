class AddAangemeldToLopers < ActiveRecord::Migration[5.1]
  def change
    add_column :lopers, :aangemeld, :boolean, default: false
  end
end
