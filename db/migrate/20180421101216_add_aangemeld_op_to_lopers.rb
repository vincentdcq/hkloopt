class AddAangemeldOpToLopers < ActiveRecord::Migration[5.1]
  def change
    add_column :lopers, :aangemeld_op, :datetime
  end
end
