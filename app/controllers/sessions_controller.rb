class SessionsController < ApplicationController
 
  def new
  end

  def create
  	home = Home.find_by(naam: params[:session][:naam])
  	if home && home.authenticate(params[:session][:password])
  		log_in home
      params[:session][:remember_me] == '1' ? remember(home) : forget(home)
      redirect_back_or home
  	else
  		flash.now[:danger] = 'Foute login!'
  		render 'new'
  	end
  end

  def destroy
  	log_out if logged_in?
    redirect_to root_url
  end
end
