class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper

  private

  def ingelogde_home
  	unless logged_in?
  		store_location
  		flash[:danger] = 'Nier ingelogd.'
  		redirect_to login_url
  	end
  end

  def admin_home
    redirect_to(root_url) unless current_user.admin?
  end
  
end
