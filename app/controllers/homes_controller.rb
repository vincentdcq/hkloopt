class HomesController < ApplicationController
  before_action :ingelogde_home, only: [:show, :edit, :update, :destroy]
  before_action :juiste_home, only: [:edit, :update]
  before_action :admin_home, only: :destroy

  def new
  	@home = Home.new
  end

  def index
    @homes = Home.paginate(page: params[:page])
  end

  def show
  	@home = Home.find(params[:id])
    @lopers = @home.lopers.paginate(page: params[:page])
    @loper = @home.lopers.build
  end

  def create
  	@home = Home.new(home_params)
  	if @home.save
  		log_in @home
  		flash[:success] = "Welkom bij de 12 urenloop!"
  		redirect_to @home
  	else
  		render 'new'
  	end
  end

  def edit
    @home = Home.find(params[:id])
  end

  def update
    @home = Home.find(params[:id])
    if @home.update_attributes(home_params)
      flash[:success] = "Update succesvol."
      redirect_to @home
    else
      render 'edit'
    end
  end

  def destroy
    Home.find(params[:id]).destroy
    flash[:success] = "Home verwijderd"
    redirect_to homes_url
  end

  def post
    @home = Home.find(params[:id])
    temp = @home.speciaal + 1
    @home.update_attribute(:speciaal, temp)
    redirect_to @home
  end

  private
  def home_params
  	params.require(:home).permit(:naam, :password, :password_confirmation)
  end

  def juiste_home
    @home = Home.find(params[:id])
    redirect_to(root_url) unless current_user?(@home)
  end
end
