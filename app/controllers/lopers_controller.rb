class LopersController < ApplicationController
	before_action :ingelogde_home, only: [:create, :destroy]
#	before_action :correcte_home, only: :destroy
	before_action :admin_home, only: [:show, :destroy]

	def create
		@loper = Loper.new(loper_params)
		if @loper.save
			flash[:success] = "Loper toegevoegd!"
			redirect_to @loper
		else
			render 'static_pages/home'
		end
	end

	def destroy
		@loper.destroy
		flash[:success] = "Loper succesvol verwijderd."
		redirect_to request.referrer || root_url
	end

	def show
		@loper = Loper.find(params[:id])
	end

	def update
		@loper = Loper.find(params[:id])
		if @loper.snelste > params[:loper][:snelste].to_i && @loper.update_attributes(update_params)
#			@loper.update_attribute(:aantal, @loper.aantal+1)
			flash[:success] = "Update succesvol."
			redirect_to @loper
		else
			render 'show'
		end
	end

	def ronde
		@loper = Loper.find(params[:id])
		@loper.update_attribute(:aangemeld, true)
		@loper.update_attribute(:aangemeld_op, DateTime.current())
		redirect_to @loper
	end

	def loop
		@loper = Loper.find(params[:id])
		if current_user && current_user.admin?
			@loper.update_attribute(:aangemeld, false)
			@loper.update_attribute(:aantal, @loper.aantal+1)
			homeid = @loper.home_id
			Home.find(homeid).update_attribute(:loopt, false)
			(-1..5).each do |i|
				index = ((homeid + i) % 7) + 2
				home = Home.find(index)
				toggled = false
				if home.lopers.where("aangemeld" => true).to_a.any?
					home.update_attribute(:loopt, true)
					toggled = true
				end
				break if toggled
			end
#			Home.find( homeid+1 > 8 ? 2 : homeid + 1 ).update_attribute(:loopt, true)
			redirect_to root_url
		else
			redirect_to root_url
		end
		
	end

	private
	def loper_params
		params.require(:loper).permit(:naam, :studentennummer, :home_id)
	end

	def update_params
		params.require(:loper).permit(:snelste)
	end

	def ronde_params
		params.require(:loper).permit(:aangemeld, :aangemeld_op)
	end

	def correcte_home
		@loper = current_user.lopers.find_by(id: params[:id])
		redirect_to root_url if @loper.nil?
	end
end
