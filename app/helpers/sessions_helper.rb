module SessionsHelper

	#logt in
	def log_in(home)
		session[:home_id] = home.id
	end

	def remember(home)
		home.remember
		cookies.permanent.signed[:home_id] = home.id
		cookies.permanent[:remember_token] = home.remember_token
	end

	def current_user?(home)
		home == current_user		
	end

	def current_user
    if (home_id = session[:home_id])
      @current_user ||= Home.find_by(id: home_id)
    elsif (home_id = cookies.signed[:home_id])
      home = Home.find_by(id: home_id)
      if home && home.authenticated?(cookies[:remember_token])
        log_in home
        @current_user = home
      end
    end
  end

	def logged_in?
		!current_user.nil?
	end

	def forget(home)
		home.forget
		cookies.delete(:home_id)
		cookies.delete(:remember_token)
	end

	def log_out
		forget(current_user)
		session.delete(:home_id)
		@current_user = nil
	end

	# Redirects to stored location (or to the default).
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # Stores the URL trying to be accessed.
  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end
end
