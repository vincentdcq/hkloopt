module HomesHelper
	def schild(home)
		schild_naam = home.naam.downcase
		schild_url = "http://www.homekonvent.be/static/images/schilden/#{schild_naam}.png"
		return image_tag(schild_url, alt: home.naam, class: "schild")
	end
end
