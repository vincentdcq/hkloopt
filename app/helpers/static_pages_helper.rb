module StaticPagesHelper
	def zoek_snelste(home)
		tijd = home.lopers.minimum("snelste")
		loper = home.lopers.find_by(snelste: tijd)
	end

	def sorteer_snelste(homes)
		lopers = Hash.new
		homes.each do |h|
			loper = zoek_snelste(h)
			if loper && loper.snelste < 1000 && loper.home.naam != "Konvent"
				lopers[loper.home] = {naam: loper.naam, tijd: loper.snelste}
			end
		end
		lopers.sort_by { |h,l| l[:tijd] }
	end

	def print_gesorteerd(gesorteerd, wat, individueel)
		html = '' 
		html << "<ol class='stand'>"

		for member in gesorteerd do
			html << content_tag(:li) do
			concat content_tag(:p, print_loper(member,wat, individueel))
			end
		end

		html << '</ol>'
		return html.html_safe
	end

	def print_loper(loper,wat,individueel)
		tekst = ""

		case individueel
		when true
			tekst << loper[0][:naam] << " - " << loper[1][:naam] << " ("
		when false
			tekst << loper[0][:naam] << " ("
		end

		case wat
		when "tijd"
			tekst << snelste_to_tijd(loper[1][:tijd]) << ")"
		when "aantal"
			aantal = -loper[1][:aantal]
			tekst << aantal.to_s << ")"
		end
	end

	def zoek_meeste_rondes(home)
		aantal = home.lopers.maximum("aantal")
		loper = home.lopers.find_by(aantal: aantal)
	end

	def sorteer_aantal(homes)
		lopers = Hash.new
		homes.each do |h|
			loper = zoek_meeste_rondes(h)
			if loper && loper.aantal > 0 && loper.home.naam != "Konvent"
				lopers[loper.home] = {naam: loper.naam, aantal: -loper.aantal}
			end
		end
		lopers.sort_by { |h,l| l[:aantal] }
	end

	def sorteer_aantal_lopers(homes)
		lopers = Hash.new
		homes.each do |h|
			lopers[h] = {naam: h.naam, aantal: -h.lopers.where("aantal > 0").count} unless h.naam == "Konvent"
		end
		lopers.sort_by { |h,l| l[:aantal] }
	end

	def sorteer_aantal_supporters(homes)
		lopers = Hash.new
		homes.each do |h|
			lopers[h] = {naam: h.naam, aantal: -h.lopers.count} unless h.naam == "Konvent"
		end
		lopers.sort_by { |h,l| l[:aantal]}
	end

	def sorteer_aantal_rondjes(homes)
		lopers = Hash.new
		homes.each do |h|
			lopers[h] = {naam: h.naam, aantal: -h.lopers.sum("aantal")} unless h.naam == "Konvent"
		end
		lopers.sort_by { |h,l| l[:aantal]}
	end

	def sorteer_speciale_rondes(homes)
		lopers = Hash.new
		homes.each do |h|
			lopers[h] = {naam: h.naam, aantal: -h.speciaal} unless h.naam == "Konvent"
		end
		lopers.sort_by { |h,l| l[:aantal]}
	end

	def sorteer_queue(lopers)
		wachtlijn = lopers.where("aangemeld" => true)
	end

	def print_queue(lopers, plaats)
		wachtlijn = sorteer_queue(lopers)
		loper = wachtlijn.order(:aangemeld_op)[plaats]
		tekst = ""
		tekst << loper.naam << " (" << loper.home.naam << ")" if loper
	end

	def get_naam_from_queue(lopers, plaats)
		wachtlijn = sorteer_queue(lopers)
		loper = wachtlijn.order(:aangemeld_op)[plaats]
		tekst = ""
		tekst << loper.naam << " (" << loper.home.naam << ")" if loper
	end

	def get_id_from_queue(lopers, plaats)
		wachtlijn = sorteer_queue(lopers)
		loper = wachtlijn.order(:aangemeld_op)[plaats]
		id = loper.id if loper
	end

	def print_queue_per_home(homes, naam)
		wachtlijn = homes.find_by(naam: naam).lopers.where("aangemeld" => true).order(:aangemeld_op).to_a
		tekst = "<ol>"
		for loper in wachtlijn do
			tekst << content_tag(:li) do
				concat content_tag(:p, loper.naam)
			end
		end
		tekst << "</ol>"
		return tekst.html_safe
	end

	def get_home_from_queue(homes)
		home = homes.where("loopt" => true).first
		home.naam if home
	end

	def get_id_from_home(homes)
		home = homes.where("loopt" => true).first
		loper = home.lopers.where("aangemeld" => true).order(:aangemeld_op).first if home
		loper.id if loper
	end

end
