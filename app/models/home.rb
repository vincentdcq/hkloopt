class Home < ApplicationRecord
  has_many :lopers, dependent: :destroy
  attr_accessor :remember_token
  validates :naam, presence: true
  has_secure_password

  #returns hash digest
  def Home.digest(string)
  	cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST : BCrypt::Engine.cost
  	BCrypt::Password.create(string, cost: cost)
  end

  def Home.new_token
  	SecureRandom.urlsafe_base64
  end

  def remember
  	self.remember_token = Home.new_token
  	update_attribute(:remember_digest, Home.digest(remember_token))
  end

  def authenticated?(remember_token)
  	return false if remember_digest.nil?
  	BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end

  def forget
    update_attribute(:remember_digest, nil)
  end
end
