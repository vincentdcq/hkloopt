class Loper < ApplicationRecord
  belongs_to :home
#  default_scope -> { order(aantal: :desc) }
  validates :home_id, presence: true
  validates :naam, presence: true
end
