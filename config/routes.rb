Rails.application.routes.draw do
  
  get 'sessions/new'

  root	'static_pages#home'
  get 	'/register',	to: 'homes#new'
  resources :homes
  resources :lopers

  get '/login',		to: 'sessions#new'
  post '/login',	to: 'sessions#create'
  delete '/logout',	to: 'sessions#destroy'

  post 'homes/:id', to: 'homes#post'
  post 'lopers/:id',to: 'lopers#ronde'
  post '/',         to: 'lopers#loop'


# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
